# include <iostream>
#include <string>
using namespace std;

struct Factory{
	string name;
	string special;
	float averageAge;
	float averageSalary;
};

int main(){
	
	setlocale(LC_ALL, "Russian");
	
	int factoryCount = 0;
	int totalSalary = 0;
	int fitterCount = 0;
	int turnerCount = 0;
	
	char key;
	char fitter;
	char turner;
	while (key != 'n'){
		Factory f;
		
		cout << "Enter name of factory:";
		cin >> f.name;
		
		cout << "Enter the average Salary:";
		cin >> f.averageSalary;
		
		cout << "Enter the average age:";
		cin >> f.averageAge;
		
		factoryCount++;
		totalSalary += f.averageSalary;
		
		cout << "Fitter? (y/n)";
		cin >> fitter;
		if (fitter == 'y'){
                   fitterCount ++;
                   }
		else{
             cout << "Turner? (y/n)";
		     cin >> turner;
		     if (turner == 'y'){
                   turnerCount ++;
                   }
                   
             }
		cout << "Do you whant to continue?(y/n)";
		cin >> key;
	}
	
	float averageSalary = totalSalary / factoryCount;
	cout << "Average salary: " << averageSalary << endl;
	cout << "Fitter count  = " << fitterCount << endl;
	cout << "Turner count  = " << fitterCount << endl;
	return 0;
}
